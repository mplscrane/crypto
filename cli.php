#!/usr/bin/env php
<?php

require "ApiCrypter.php";


$ApiCrypter = new ApiCrypter();

if($argc < 3) {
    die("usage : ./cli [enc|dec] \"your string\" \n");
} else {
    if(trim($argv[1]) == "enc") {
        $enc = $ApiCrypter->encrypt(trim($argv[2]));
        echo $enc;
        echo "\n";
    }
    if(trim($argv[1]) == "dec") {
        $dec = $ApiCrypter->decrypt(trim($argv[2]));
        echo $dec;
        echo "\n";
    }
}



?>
